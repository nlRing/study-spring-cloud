/**   
* @Title: WeatherReportServiceImpl.java 
* @Package com.yao.spring.cloud.weather.service.impl 
* @Description: TODO(用一句话描述该文件做什么) 
* @author yaoym
* @date 2018年3月23日 下午3:30:25 
* @version V1.0   
*/
package com.yao.spring.cloud.weather.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yao.spring.cloud.weather.service.WeatherDataService;
import com.yao.spring.cloud.weather.service.WeatherReportService;
import com.yao.spring.cloud.weather.vo.Weather;
import com.yao.spring.cloud.weather.vo.WeatherResponse;

/**
 * @Description: 天气预报服务实现类
 * @author yaoym
 * @date 2018年3月23日 下午3:30:25
 * @version V1.0
 */
@Service
public class WeatherReportServiceImpl implements WeatherReportService {

	@Autowired
	WeatherDataService weatherDataService;

	@Override
	public Weather getDataByCityID(String cityID) {

		WeatherResponse response = weatherDataService.getDataByCityID(cityID);

		return response.getData();
	}

}
