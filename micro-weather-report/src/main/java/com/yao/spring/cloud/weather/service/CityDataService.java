/**   
* @Title: CityDataService.java 
* @Package com.yao.spring.cloud.weather.service 
* @Description: TODO(用一句话描述该文件做什么) 
* @author yaoym
* @date 2018年3月23日 上午9:26:45 
* @version V1.0   
*/
package com.yao.spring.cloud.weather.service;

import java.util.List;

import com.yao.spring.cloud.weather.vo.City;

/**   
 * @Description: 
 * @author yaoym
 * @date 2018年3月23日 上午9:26:45 
 * @version V1.0
 */
public interface CityDataService {

	/**
	 * @Description: 获取城市列表 
	 * @since 2018年3月23日 上午9:27:28 
	 * @author yaoym
	 * @return
	 * @throws Exception
	 */
	List<City> listCity() throws Exception;
}
