package com.yao.spring.cloud.weather.job;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.yao.spring.cloud.weather.service.CityDataService;
import com.yao.spring.cloud.weather.service.WeatherDataService;
import com.yao.spring.cloud.weather.vo.City;

/**
 * 天气数据 同步 定时器
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author yaoym
 * @date 2018年3月22日 下午5:46:15 
 * @version V1.0
 */
public class WeatherDataSyncJob extends QuartzJobBean {
	
	private static final Logger logger = LoggerFactory.getLogger(WeatherDataSyncJob.class);
	
	@Autowired
	private CityDataService cityDataService;

	@Autowired
	private WeatherDataService weatherDataService;

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		logger.info("---天气数据开始同步---");
		
		List<City> citylist = null;
		//获取城市列表
		try {
			citylist = cityDataService.listCity();
		} catch (Exception e) {
			logger.error("---获取城市列表出错---",e.getStackTrace());
		}
		
		//遍历城市id获取数据
		citylist.stream().forEach(city -> {
			String cityID = city.getCityID();
			logger.info("---当前城市id为：{}",cityID);
			weatherDataService.syncDataByCityID(cityID);
		});

		logger.info("---天气数据同步结束---");
	}

}
