package com.yao.spring.cloud.weather.service;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.yao.spring.cloud.weather.vo.City;

@FeignClient("msa-weather-city-eureka")
public interface CityClient {

	@GetMapping("/cities/list")
	List<City> list();
}
