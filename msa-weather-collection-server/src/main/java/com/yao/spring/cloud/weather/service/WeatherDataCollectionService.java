/**   
* @Title: WeatherDataCollectionService.java 
* @Package com.yao.spring.cloud.weather.service 
* @Description: TODO(用一句话描述该文件做什么) 
* @author yaoym
* @date 2018年3月26日 下午2:43:12 
* @version V1.0   
*/
package com.yao.spring.cloud.weather.service;

/**   
 * @Description: 天气数据采集服务
 * @author yaoym
 * @date 2018年3月26日 下午2:43:12 
 * @version V1.0
 */
public interface WeatherDataCollectionService {

	/**
	 * 根据城市id同步数据
	 * @Description: 
	 * @since 2018年3月26日 下午2:43:59 
	 * @author yaoym
	 * @param cityID
	 */
	void syncDataByCityID(String cityID);
	
}
