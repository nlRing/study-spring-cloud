package com.yao.spring.cloud.weather.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Rest Configuration
 * 
 * @Description: TODO(用一句话描述该文件做什么)
 * @author yaoym
 * @date 2018年3月20日 下午3:21:51
 * @version V1.0
 */
@Configuration
public class RestConfiguration {

	@Autowired
	private RestTemplateBuilder builder;

	@Bean
	public RestTemplate restTemp() {
		return builder.build();
	}
}
