package com.yao.spring.cloud.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Description: 城市服务
 * @author yaoym
 * @date 2018年3月27日 下午3:30:45
 * @version V1.0
 */
@FeignClient("msa-weather-city-eureka")
public interface CityClient {

	@RequestMapping(value = "/cities/list", method = RequestMethod.GET)
	String listCity();
}
