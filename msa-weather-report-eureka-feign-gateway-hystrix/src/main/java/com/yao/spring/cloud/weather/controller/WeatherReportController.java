/**   
* @Title: WeatherReportController.java 
* @Package com.yao.spring.cloud.weather.controller 
* @Description: TODO(用一句话描述该文件做什么) 
* @author yaoym
* @date 2018年3月23日 下午3:32:56 
* @version V1.0   
*/
package com.yao.spring.cloud.weather.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.yao.spring.cloud.weather.service.DataClient;
import com.yao.spring.cloud.weather.service.WeatherReportService;

/**
 * @Description: 天气预报接口供应商
 * @author yaoym
 * @date 2018年3月23日 下午3:32:56
 * @version V1.0
 */
@RestController
@RequestMapping(value = "/report")
public class WeatherReportController {

	@Autowired
	private WeatherReportService weatherReportService;

	@Autowired
	private DataClient dataClient;

	@GetMapping("/cityId/{cityId}")
	public ModelAndView getReportByCityID(@PathVariable("cityId") String cityID, Model model) throws Exception {

		model.addAttribute("title", "天气预报系统");
		model.addAttribute("cityId", cityID);
		model.addAttribute("cityList", dataClient.list());
		model.addAttribute("report", weatherReportService.getDataByCityID(cityID));

		return new ModelAndView("weather/report", "reportModel", model);

	}
}
