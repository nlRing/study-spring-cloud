/**   
* @Title: CityDataServiceImpl.java 
* @Package com.yao.spring.cloud.weather.service.impl 
* @Description: TODO(用一句话描述该文件做什么) 
* @author yaoym
* @date 2018年3月23日 上午9:27:56 
* @version V1.0   
*/
package com.yao.spring.cloud.weather.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.yao.spring.cloud.weather.service.CityDataService;
import com.yao.spring.cloud.weather.util.XmlBuilder;
import com.yao.spring.cloud.weather.vo.City;
import com.yao.spring.cloud.weather.vo.CityList;

/**
 * @Description: 城市服务实现
 * @author yaoym
 * @date 2018年3月23日 上午9:27:56
 * @version V1.0
 */
@Service
public class CityDataServiceImpl implements CityDataService {

	@Override
	public List<City> listCity() throws Exception {

		// 读取city xml文件
		Resource resource = new ClassPathResource("citylist.xml");

		BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream(), "UTF-8"));

		StringBuffer buffer = new StringBuffer();
		String line = "";

		while ((line = br.readLine()) != null) {
			buffer.append(line);
		}

		if (br != null) {
			br.close();
		}

		CityList citylist = (CityList) XmlBuilder.xmlStrToObject(CityList.class, buffer.toString());

		return citylist.getCityList();
	}

}
