package com.yao.spring.cloud.weather.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yao.spring.cloud.weather.service.WeatherDataService;
import com.yao.spring.cloud.weather.vo.WeatherResponse;

/**
 * Weather Controller
 * 
 * @Description: TODO(用一句话描述该文件做什么)
 * @author yaoym
 * @date 2018年3月20日 下午3:17:42
 * @version V1.0
 */
@RestController
@RequestMapping(value = "/weather")
public class WeatherController {

	@Autowired
	WeatherDataService weatherDataService;

	@GetMapping("/cityId/{cityId}")
	public WeatherResponse getWeatherByCityID(@PathVariable("cityId") String cityID) {
		return weatherDataService.getDataByCityID(cityID);
	}

	@GetMapping("/cityName/{cityName}")
	public WeatherResponse getWeatherByCityName(@PathVariable("cityName") String cityName) {
		return weatherDataService.getDataByCityName(cityName);

	}
}
