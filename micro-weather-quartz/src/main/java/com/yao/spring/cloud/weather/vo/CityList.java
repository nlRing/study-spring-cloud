/**   
* @Title: CityList.java 
* @Package com.yao.spring.cloud.weather.vo 
* @Description: TODO(用一句话描述该文件做什么) 
* @author yaoym
* @date 2018年3月23日 上午9:18:34 
* @version V1.0   
*/
package com.yao.spring.cloud.weather.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @Description: 城市列表
 * @author yaoym
 * @date 2018年3月23日 上午9:18:34
 * @version V1.0
 */
@XmlRootElement(name = "c")
@XmlAccessorType(XmlAccessType.FIELD)
public class CityList {

	@XmlElement(name = "d")
	private List<City> cityList;

	public List<City> getCityList() {
		return cityList;
	}

	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}

}
