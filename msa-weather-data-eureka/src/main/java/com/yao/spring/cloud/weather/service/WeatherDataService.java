package com.yao.spring.cloud.weather.service;

import com.yao.spring.cloud.weather.vo.WeatherResponse;

/**
 * 天气数据接口
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author yaoym
 * @date 2018年3月20日 下午3:04:42 
 * @version V1.0
 */
public interface WeatherDataService {


	/**
	 * 根据城市id查询天气数据
	 * @Description: TODO(这里用一句话描述这个类的作用) 
	 * @since 2018年3月20日 下午3:05:24 
	 * @author yaoym
	 * @param cityID
	 * @return
	 */
	WeatherResponse getDataByCityID(String cityID);
	
	/**
	 * 根据城市名称查询天气数据
	 * @Description: TODO(这里用一句话描述这个类的作用) 
	 * @since 2018年3月20日 下午3:05:59 
	 * @author yaoym
	 * @param cityName
	 * @return
	 */
	WeatherResponse getDataByCityName(String cityName);
	
}
